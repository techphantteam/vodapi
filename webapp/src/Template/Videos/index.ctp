<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Video'), ['action' => 'add']) ?></li>
    </ul>
</div>
<div class="videos index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('userid') ?></th>
            <th><?= $this->Paginator->sort('countryid') ?></th>
            <th><?= $this->Paginator->sort('video_title') ?></th>
            <th><?= $this->Paginator->sort('video_director') ?></th>
            <th><?= $this->Paginator->sort('video_synopsis') ?></th>
            <th><?= $this->Paginator->sort('country') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($videos as $video): ?>
        <tr>
            <td><?= $this->Number->format($video->id) ?></td>
            <td><?= $this->Number->format($video->userid) ?></td>
            <td><?= $this->Number->format($video->countryid) ?></td>
            <td><?= h($video->video_title) ?></td>
            <td><?= h($video->video_director) ?></td>
            <td><?= h($video->video_synopsis) ?></td>
            <td><?= h($video->country) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $video->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $video->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $video->id], ['confirm' => __('Are you sure you want to delete # {0}?', $video->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
