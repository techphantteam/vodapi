<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $video->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $video->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Videos'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="videos form large-10 medium-9 columns">
    <?= $this->Form->create($video) ?>
    <fieldset>
        <legend><?= __('Edit Video') ?></legend>
        <?php
            echo $this->Form->input('userid');
            echo $this->Form->input('genres');
            echo $this->Form->input('topics');
            echo $this->Form->input('tags');
            echo $this->Form->input('countryid');
            echo $this->Form->input('video_title');
            echo $this->Form->input('video_year');
            echo $this->Form->input('video_description');
            echo $this->Form->input('video_director');
            echo $this->Form->input('video_synopsis');
            echo $this->Form->input('country');
            echo $this->Form->input('film_festival');
            echo $this->Form->input('language');
            echo $this->Form->input('video_logline');
            echo $this->Form->input('video_cast');
            echo $this->Form->input('video_link');
            echo $this->Form->input('video_length');
            echo $this->Form->input('video_size');
            echo $this->Form->input('video_file_type');
            echo $this->Form->input('channel');
            echo $this->Form->input('video_trailer_url');
            echo $this->Form->input('video_twitter');
            echo $this->Form->input('video_fbpage');
            echo $this->Form->input('video_referal');
            echo $this->Form->input('total_like');
            echo $this->Form->input('total_share');
            echo $this->Form->input('is_hd_sd');
            echo $this->Form->input('is_paid');
            echo $this->Form->input('price');
            echo $this->Form->input('is_approved');
            echo $this->Form->input('need_signup');
            echo $this->Form->input('modidfied');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
