<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
/**
 * VimeoUsers Controller
 *
 * @property \App\Model\Table\VimeoUsersTable $VimeoUsers
 */
class VimeoUsersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
       if ($this->request->is('post')) {
          if ($this->isValidRequest($this->request->data)) {
           unset($this->request->data['client_id']);
           unset($this->request->data['hash_value']);
           if($getvimeo=$this->VimeoUsers->find('all',['order'=>'id'])){
             $vimeouserdt=array(); 
             $count=0;  
             $value=array('id'=>array(),'cid'=>array(),'client_secret'=>array(),'client_access_token'=>array(),'type'=>array(),'active'=>array());
            foreach ($getvimeo as $vimeokey => $vimeovalue) {
                    $value['id'][]=$vimeovalue->get('id');
                    $value['cid'][]=$vimeovalue->get('cid');
                    $value['client_secret'][]=$vimeovalue->get('client_secret');
                    $value['client_access_token'][]=$vimeovalue->get('client_access_token');
                    $value['type'][]=$vimeovalue->get('type');
                    $value['active'][]=$vimeovalue->get('active');
                }
               // $vimeouserdt=array($id,$userid,$cid,$client_secret,$client_access_token,$type,$active);
                $message = "Success";
                $this->set([
                'code'=>'200',    
                'msgstatus'=>Configure::read('status.200'),    
                'message' => $message,
                'msgstatus' => $value,
                '_serialize' => ['message','userdata','msgstatus','code']]);    
           }else{
                    $message = 'Error';
                    $this->set([
                    'code'=>'406',
                    'msgstatus'=>Configure::read('status.406'),    
                    'message' => $message,  
                    '_serialize' => ['message','msgstatus','code']]);
           } 
          }else{
                 $message = "Invalid operation";
                 $this->set([
                 'code'=>'404',
                 'msgstatus'=>Configure::read('status.404'),
                 'message' => $message,
                 '_serialize' => ['message','msgstatus','code']
                ]);
           
          }   
        }else{
                 $message = "Invalid request";
                 $this->set([
                 'code'=>'400',
                 'msgstatus'=>Configure::read('status.400'),
                 'message' => $message,
                 '_serialize' => ['message','msgstatus','code']
                ]);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Vimeo User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vimeoUser = $this->VimeoUsers->get($id, [
            'contain' => []
        ]);
        $this->set('vimeoUser', $vimeoUser);
        $this->set('_serialize', ['vimeoUser']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vimeoUser = $this->VimeoUsers->newEntity();
        if ($this->request->is('post')) {
           
            if ($this->isValidRequest($this->request->data)) {

                unset($this->request->data['client_id']);
                unset($this->request->data['hash_value']);
                $vimeoUser = $this->VimeoUsers->patchEntity($vimeoUser, $this->request->data);
                if ($this->VimeoUsers->save($vimeoUser)) {
                    $message = 'Saved';
                    $this->set([
                    'code'=>'200',    
                    'msgstatus'=>Configure::read('status.200'),    
                    'message' => $message,
                    'userdata' => $vimeoUser,
                    '_serialize' => ['message','errormessage','userdata','msgstatus','code']
                ]);
                } else {
                    $errmessage="";
                    foreach ($vimeoUser->errors() as $userkey => $value) {
                       $errmessage[$userkey]=$value;
                    }
                    $message = 'Error';
                    $this->set([
                    'code'=>'406',
                    'msgstatus'=>Configure::read('status.406'),    
                    'message' => $message,  
                    'userdata' => $vimeoUser,
                    'errormessage'=>$errmessage,
                    '_serialize' => ['message','errormessage','userdata','msgstatus','code']
                ]);
                } 

             }else{

                $message = "Invalid operation";
                   $this->set([
                        'code'=>'404',
                        'msgstatus'=>Configure::read('status.404'),
                        'message' => $message,
                        '_serialize' => ['message','msgstatus','code']
                    ]);
             }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Vimeo User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vimeoUser = $this->VimeoUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vimeoUser = $this->VimeoUsers->patchEntity($vimeoUser, $this->request->data);
            if ($this->VimeoUsers->save($vimeoUser)) {
                $this->Flash->success(__('The vimeo user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The vimeo user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('vimeoUser'));
        $this->set('_serialize', ['vimeoUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vimeo User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vimeoUser = $this->VimeoUsers->get($id);
        if ($this->VimeoUsers->delete($vimeoUser)) {
            $this->Flash->success(__('The vimeo user has been deleted.'));
        } else {
            $this->Flash->error(__('The vimeo user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
