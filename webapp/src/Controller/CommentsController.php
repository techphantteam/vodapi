<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Http\Message;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\I18n\Time;
/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 */
class CommentsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
     public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    public function index()
    {
        $this->paginate = [
            'contain' => ['Videos', 'Users', 'ParentComments']
        ];
        $this->set('comments', $this->paginate($this->Comments));
        $this->set('_serialize', ['comments']);
    }

    /**
     * View method
     *
     * @param string|null $id Comment id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => ['Videos', 'Users', 'ParentComments']
        ]);
        $this->set('comment', $comment);
        $this->set('_serialize', ['comment']);
    }
    public function commentData($id = null)
    {
        if ($this->request->is('post')) {
            if ($this->isValidRequest($this->request->data)) {
                if ($data = $this->Comments->find('all')
                        ->where(['videos_id' => $this->request->data['videos_id']])
                        ->order(['created_date' => 'DESC'])) {
                    $d1 = array();
                    foreach ($data as $row) {
                        $d1[]= $row;
                    }
                    $response['code'] = "200";
                    $response['data'] = $d1;
                } else {
                    //$this->Flash->error(__('The video could not be saved. Please, try again.'));
                    $response['code'] = "406";
                    $response['msg'] = "Error in Get Video Data!";
                    $response['errors'] = $video->errors();
                    $response['data'] = $this->request->post;
                }
            } else {
                $response['code'] = "401";
                $response['msg'] = "Unauthorized Request!";
            }
        } else {
            $response['code'] = "400";
            $response['msg'] = "Bad Request!";
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);

    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $comment = $this->Comments->newEntity($this->request->data,['validate' => false]);
        
        if ($this->request->is('post')) {
           
            if ($this->isValidRequest($this->request->data)) {
               
                unset($this->request->data['client_id']);
                unset($this->request->data['hash_value']);
                $comment = $this->Comments->patchEntity($comment, $this->request->data, ['validate' => false]);
                if ($id=$this->Comments->save($comment)) {
                    $message = "Success";
                    $this->set([
                        'code'=>'200',       
                        'message' => $message,
                        'id' => $id->id,
                        'name' => $this->request->data['name'],
                        'comments' => $this->request->data['comments'],
                        '_serialize' => ['message','comments','code','id','name']]); 
                  
                } else {
                    $errmessage="";
                    foreach ($comment->errors() as $userkey => $value) {
                       $errmessage[$userkey]=$value;
                    }
                    $message = 'Error';
                    $this->set([
                    'code'=>'406',
                    'msgstatus'=>Configure::read('status.406'),    
                    'message' => $message,  
                    'errormessage'=>$errmessage,
                    '_serialize' => ['message','errormessage','userdata','msgstatus','code']
                ]);
                }
            }
            else{
                $this->invalidoperation();

             } 
        }
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Comment id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $comment = $this->Comments->patchEntity($comment, $this->request->data);
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }
        $videos = $this->Comments->Videos->find('list', ['limit' => 200]);
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $parentComments = $this->Comments->ParentComments->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'videos', 'users', 'parentComments'));
        $this->set('_serialize', ['comment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $comment = $this->Comments->get($id);
        if ($this->Comments->delete($comment)) {
            $this->Flash->success(__('The comment has been deleted.'));
        } else {
            $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
